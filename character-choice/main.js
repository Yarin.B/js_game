import { charactersArray1, charactersArray2 } from "./players.js"

export let player1Image = document.getElementById("player-image-1");
export let player2Image = document.getElementById("player-image-2");
let player2Left = document.getElementById("player2ButtonLeft");
let player2Right = document.getElementById("player2ButtonRight");
let player1Left = document.getElementById("player1ButtonLeft");
let player1Right = document.getElementById("player1ButtonRight");
let playerIndex1 = 0;
let playerIndex2 = 0;
// export let source2 = "../head-soccer-game/images/characters/player-1/Character3.png";
// export let source1 = "../head-soccer-game/images/characters/player-1/Character8.png";
player1Image.src = charactersArray1[playerIndex1];
player2Image.src = charactersArray2[playerIndex2];

player1Left.onclick = playerOneLeftButton;
player1Right.onclick = playerOneRightButton;
player2Left.onclick = playerTwoLeftButton;
player2Right.onclick = playerTwoRightButton;

function playerTwoRightButton() {
    playerIndex2++;
    player2Image.src = charactersArray2[playerIndex2];
    if (playerIndex2 > charactersArray2.length) playerIndex2 = 0;
}

function playerTwoLeftButton() {
    playerIndex2--;
    player2Image.src = charactersArray2[playerIndex2];
    if (playerIndex2 < 0) playerIndex2 = charactersArray2.length;
}

function playerOneRightButton() {
    playerIndex1++;
    player1Image.src = charactersArray1[playerIndex1];
    if (playerIndex1 > charactersArray1.length) playerIndex1 = 0;
    console.log(player1Image.src);
}

function playerOneLeftButton() {
    playerIndex1--;
    player1Image.src = charactersArray1[playerIndex1];
    if (playerIndex1 < 0) playerIndex1 = charactersArray1.length;
    console.log(player1Image.src);
}


export let playerOne = "images/characters/player-1/character14.png";
export let playerTwo = "images/characters/player-2/character18.png";